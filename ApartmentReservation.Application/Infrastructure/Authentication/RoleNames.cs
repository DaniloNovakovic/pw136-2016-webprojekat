﻿namespace ApartmentReservation.Application.Infrastructure.Authentication
{
    public static class RoleNames
    {
        public const string Administrator = "Administrator";
        public const string Host = "Host";
        public const string Guest = "Guest";
    }
}