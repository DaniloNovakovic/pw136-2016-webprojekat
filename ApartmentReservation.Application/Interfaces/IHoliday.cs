﻿namespace ApartmentReservation.Application.Interfaces
{
    public interface IHoliday
    {
        int Day { get; set; }
        int Month { get; set; }
    }
}